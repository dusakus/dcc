/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc;

import java.util.ArrayList;

/**
 *
 * @author bartek
 */
public class DCExitH {

    ArrayList<ICloseable> list;

    public DCExitH() {
        list = new ArrayList<>();
    }

    public void add(ICloseable i) {
        list.add(i);
    }

    public void EXIT(int out) {
        for (ICloseable i : list) {
            i.CLOSE(out);
        }
        System.exit(out);
    }
}
