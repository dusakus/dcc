package dcc;

import dcc.dcfg.CFG;
import dcc.sApp.DCec;
import dcc.sApp.Loop;
import dcc.sApp.MTcore;
import java.io.File;

/**
 *
 * @author dusakus
 *
 * This is going to be a static data storage, so i won't need to pass so many
 * things betwen objects/methods
 */
public class StData {

    public static DCoutputH log;
    public static CFG coreCfg;
    public static File homeDir;
    public static Loop mainLoop;
    public static MTcore mainTM;
    public static Object app;
    private static info_DC DCCVer;

    public static void setLog(DCoutputH a) {
        log = a;
    }

    public static void setHomeDir(File a) {
        homeDir = a;
    }

    public static void GO(Object b, Object a, boolean MT) {
        app = b;
        if (MT) {
            mainTM = (MTcore) a;
        } else {
            mainLoop = (Loop) a;
            mainLoop.add((DCec) b);
            mainLoop.start(-1);
        }
    }

    public static void setCC(CFG a) {
        coreCfg = a;
    }

    public static info_DC getDCCVer() {
        return DCCVer;
    }
}
