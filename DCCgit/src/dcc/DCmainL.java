package dcc;

import dcc.sApp.DCec;
import dcc.sApp.parts.TInfo;
import dcc.sify.boxes.LongB;

/**
 *
 * @author dusakus
 */
public class DCmainL extends DCec {

    int a = 0;
    DCoutputH log = StData.log;

    @Override
    public boolean init() {
        INFO = new TInfo();
        log.println("Init phrase done", "q");
        return true;
    }

    @Override
    public boolean tick() {
        return false;
    }

    @Override
    public boolean stop() {
        return true;
    }

}
