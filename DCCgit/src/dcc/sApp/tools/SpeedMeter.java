/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.sApp.tools;

import dcc.DCoutputH;
import dcc.sApp.DCec;
import dcc.sify.MultiString;

/**
 *
 * @author bartek
 */
public class SpeedMeter extends DCec {

    DCoutputH log;
    private boolean enabled = true;
    private boolean isMessage;
    private long counter;
    private boolean forceMessage;
    private long avg = 0;
    private long lastNanoL = 0L;
    private long lastNano = 0L;
    private int TPS = 0;
    private long counter2;
    private long lastNanoM;

    public SpeedMeter(DCoutputH l) {
        log = l;
    }

    public synchronized boolean accesEnabled(boolean edit, boolean val) {
        if (edit) {
            enabled = val;
        }
        return enabled;
    }

    public void disable() {
        accesEnabled(true, false);
    }

    public void enable() {
        accesEnabled(true, true);
    }

    public boolean userTick() {
        counterT();
        if (enabled && isMessage) {
            message();
        }
        if (forceMessage){
            message();
            forceMessage = false;
        }
        if((System.nanoTime() - lastNanoL) >= 1000000000L){
            TPS = (int) counter2;
            counter2 = 0;
            lastNanoL = System.nanoTime();
        }
        lastNanoM = System.nanoTime();
        return false;

    }

    private void counterT() {
        counter++;
        counter2++;
        long currentNano = System.nanoTime();
        long took = (currentNano - lastNano);
        avg = (took + avg*(counter))/(counter+1);
        if (counter == INFO.loop.getTPSLimit()-1) {
            isMessage = true;
        }
        if (counter >= INFO.loop.getTPSLimit()) {
            counter = 0;
            avg = 0;
        }
    }

    private void message() {
        MultiString msg = new MultiString("sApp-tools-SpeedControl: measurements for tick No. "+INFO.loop.getTCount());
        msg.addL("times ticked: "+this.getTCount());
        msg.addL("speed limit is (TPS): "+INFO.loop.getTPSLimit());
        msg.addL("and current TPS is: "+TPS);
        msg.addL("Average tick takes (ns) "+avg);
        msg.addL("last tick was "+ (System.nanoTime()-lastNanoM));
        msg.addL("And last active tick time is (ns) "+ INFO.loop.getLastActiveTime());
        log.print("["+INFO.loop.getTCount()+"]", msg);
        isMessage = false;
        forceMessage = false;
    }
    public void forceMessage(){
        forceMessage = true;
    }
}
