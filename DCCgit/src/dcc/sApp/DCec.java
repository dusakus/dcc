package dcc.sApp;

import dcc.DCoutputH;
import dcc.sApp.parts.TInfo;

/**
 * @author dusakus
 */
public class DCec implements DCif, Runnable {

    public String name = "untitled";
    public TInfo INFO = new TInfo();
    private boolean RUN = false;
    private boolean STOP = false;
    private boolean INIT = false;
    private long ID;
    private long IcOUNTER;

    @Override
    public boolean tick() {
        IcOUNTER++;
        return userTick();

    }

    @Override
    public boolean init() {
        INFO.name = name;
        INFO.thrd = this;
        INIT = true;
        RUN = true;
        return userInit();

    }

    @Override
    public boolean stop() {
        RUN = false;
        STOP = true;
        return userStop();
    }

    @Override
    public void setID(long ID) {
        this.ID = ID;
        INFO.id.a = ID;
    }

    @Override
    public long getID() {
        return ID;
    }

    @Override
    public String getName() {
        return name;
    }
    public long getTCount(){
        return IcOUNTER;
    }

    public boolean isRunning() {
        return RUN;
    }

    public boolean isStopped() {
        return STOP;
    }

    @Override
    public void run() {
        if (!INIT) {
            init();
        }
        while (RUN) {
                tick();
        }
    }

    public boolean userInit() {
        return false;
    }

    public boolean userStop() {
        return false;
    }

    public boolean userTick() {
        return false;
    }
}
