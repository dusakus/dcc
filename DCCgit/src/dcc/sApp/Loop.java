package dcc.sApp;

import dcc.DCoutputH;
import dcc.sApp.parts.TickMe;
import dcc.sify.boxes.LongB;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Loop extends DCec {

    public boolean isRunning = false;
    private int tps = 0;
    private DCoutputH log;
    private TickMe tbt;
    private long eticks = 0L;
    private long LAtime = 0L;

    public Loop(DCoutputH logI) {
        log = logI;
        tps = 100;
        tbt = new TickMe(log, this);
        log.println("Tick system loaded", "D");
    }

    public Loop(DCoutputH logI, int speed) {
        log = logI;
        tps = speed;
        tbt = new TickMe(log, this);
        log.println("Tick system loaded", "D");
    }

    public void start(long max) {
        log.println("Loop just started", "D");
        long ticks = 0;
        long started = System.currentTimeMillis();

        // OLD Speed controll variables
        /*long last = System.currentTimeMillis();
         int now;
         final int planned = 1000 / tps;
         int tick;
         isRunning = true;*/
        //NEW speed control variables
        long currentNano = 0;
        long took = 0;
        long lastNano = 0;
        long plannedTime = 0;
        if (tps>0) {
            lastNano = System.nanoTime();
            currentNano = System.nanoTime();
            plannedTime = 1000000000L / tps;
            took = 0L;
            int delay = 0;
        }
        

        isRunning = true;
        while (isRunning) {
            //OLD Sped controll system ;)
            /*now = (int) (System.currentTimeMillis() - last);
             last = System.currentTimeMillis();
             tick = planned - now;
             if (tick > 0) {
             try {
             Thread.sleep(tick);
             } catch (Exception e) {
             log.println("Error in Loop, failed to sleep for " + tick + "ms", "E1");
             }
             } else {
             }*/
            //NEW speed controll system
            if (tps>0) {
                currentNano = System.nanoTime();
                took = currentNano - lastNano;
                if (took < plannedTime) {
                    try {
                        int sT = (int) ((plannedTime - took) / 1000000);
                        if (sT == 0) {
                            sT = 1;
                        }
                        sleep(sT);
                    } catch (InterruptedException ex) {
                        log.println("sApp-Loop: limiter interrupted at " + ticks, "D");
                    }
                }
                if (took > (10 * plannedTime)) {
                    log.println("sApp-Loop: performance got preety low at " + ticks, "D");
                }
                lastNano = System.nanoTime();
            }
            //real tick here:
            long TTa = System.nanoTime();
            if(tbt == null)
                break;
            if (tick() == true) {
                break;
            }
            LAtime = System.nanoTime() - TTa;
            //END if finished
            if (max > 0) {
                if (max <= ticks) {
                    break;
                }
            }
            ticks++;

        }
        if (tbt != null) {
            tbt.tickE();
        }
        log.println("Loop just Stopped, it did " + ticks + " ticks, and worked for " + (System.currentTimeMillis() - started) + " miliseconds", "D");
    }

    @Override
    public boolean tick() {
        eticks++;
        return tbt.tick();
    }

    public void kill(LongB tk) {
        tbt.killQ.add(tk);
    }

    public LongB add(DCec in) {
        return tbt.Add(in);
    }

    @Override
    public boolean init() {
        return false;
    }

    @Override
    public boolean stop() {
        tbt.tickE();
        tbt = null;
        isRunning = false;
        return false;
    }
    public int getTPSLimit(){
        return tps;
    }
    public long getTCount(){
        return eticks;
    }

    public long getLastActiveTime() {
        return LAtime;
    }
}
