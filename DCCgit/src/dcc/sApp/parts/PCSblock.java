package dcc.sApp.parts;

/**
 *
 * @author dusakus
 *
 * this class is a single imformation block used in Process comunication system
 */
public class PCSblock {

    public boolean done = false;
    public String sender = "unknown sender";
    public String receiver = "null";
    public String title = "unnamed";
    public String value = "unknown";
    public Object object = null;

}
