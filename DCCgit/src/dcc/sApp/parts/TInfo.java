package dcc.sApp.parts;

import dcc.sApp.DCec;
import dcc.sApp.DCif;
import dcc.sApp.Loop;
import dcc.sify.boxes.LongB;

/**
 *
 * @author dusakusD
 */
public class TInfo {

    public String name = "Unknown Object";
    public LongB id;
    public DCec thrd;
    public boolean exit;
    public Loop loop;

    public TInfo() {
    }

    public TInfo(String name, LongB id, DCec proc, Loop l) {
        this.name = name;
        this.id = id;
        this.thrd = proc;
        loop = l;
    }

}
