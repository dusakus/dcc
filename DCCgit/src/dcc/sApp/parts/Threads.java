package dcc.sApp.parts;

import dcc.DCoutputH;
import dcc.sApp.DCec;
import dcc.sApp.DCif;
import dcc.sify.boxes.LongB;
import java.util.ArrayList;

/**
 * @author dusakus
 */
public class Threads {

    DCoutputH log;
    private ArrayList<TInfo> tbt;
    private ArrayList<TInfo> addQueue = new ArrayList<TInfo>();
    public ArrayList<LongB> killQ = new ArrayList<LongB>();

    public Threads(DCoutputH logI) {
        log = logI;
        this.tbt = new ArrayList<TInfo>();
    }

    public synchronized void Add(TInfo in) {
        addQueue.add(in);
        in.thrd.init();
        long a = System.currentTimeMillis();
        in.thrd.setID(a);
    }

}
