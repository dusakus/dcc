package dcc.sApp;

import dcc.StData;
import dcc.sApp.parts.PCSblock;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author dusakus
 *
 * This class is used to comunicate betwen processes/threads
 */
public class ProCS implements DCif {

    Queue<PCSblock> queue;
    boolean isLoop;

    public ProCS(boolean IisLoop) {
        queue = new LinkedList<PCSblock>();
        isLoop = IisLoop;
        StData.log.println("New Bus created", "D");

    }

    //adding elements
    public synchronized void add(PCSblock block) {
        queue.add(block);
    }

    //accessing the queue
    public synchronized PCSblock ifNextGet(String type) {
        if (queue.peek().receiver.equals(type)) {
            return queue.poll();
        } else {
            return null;
        }
    }

    public synchronized PCSblock ifNextCopy(String type) {
        if (queue.peek().receiver.equals(type)) {
            return queue.peek();
        } else {
            return null;
        }
    }

    //These aren't recommended
    public synchronized PCSblock NextGet() {
        return queue.poll();
    }

    public synchronized PCSblock NextCopy() {
        return queue.peek();
    }

    //For InLoop work
    @Override
    public boolean tick() {
        if (!queue.isEmpty()) {
            if (queue.peek().done) {
                queue.remove();
            }
            if (queue.peek().receiver.equalsIgnoreCase("bus")) {
                if (queue.peek().title.equalsIgnoreCase("stop")) {
                    queue.clear();
                    StData.log.println("Loop stopping as asked by " + queue.peek().sender, "D");
                    StData.log.println("Message: " + queue.peek().value, "D");
                    return true;

                } else {
                    queue.remove();
                }
            }
        }
        return false;

    }

    @Override
    public boolean init() {
        StData.log.println("Bus appended to Loop", "D");
        return false;

    }

    @Override
    public boolean stop() {
        queue.clear();
        return false;
    }

    @Override
    public void setID(long ID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long getID() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
