/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.dcfg2;

import dcc.DCoutputH;
import dcc.dcfg2.nodes.Node;
import dcc.dcfg2.nodes.NodeB;
import dcc.dcfg2.nodes.NodeD;
import dcc.dcfg2.nodes.NodeI;
import dcc.dcfg2.nodes.NodeS;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dusakus
 */
public class CFG {

    public static DCoutputH LOG;

    private Block baseBlock;
    private Block currentBlock;
    private boolean changed;
    public boolean locked;
    private long currentLock;

    public CFG(DCoutputH l) {
        locked = false;
        LOG = l;
        baseBlock = new Block();
        baseBlock.setName("BASE");
        currentBlock = baseBlock;
    }

    public CFG cd(String name) {
        long lock = lock();

        if (name.equals("..")) {
            currentBlock = currentBlock.getParrent();
            if (currentBlock == null) {
                currentBlock = baseBlock;
            }
            unlock(lock);
            return this;
        }
        if (name.equals("/")) {
            currentBlock = baseBlock;
            unlock(lock);
            return this;
        }
        if (!name.startsWith("/")) {
            Node n = currentBlock.get(name.replace('/', ' ').trim(), 'X');
            if (n == null) {
                n = new Block();
                n.setName(name.replace('/', ' ').trim());
                currentBlock.add(n, false);
                changed();
            }
            currentBlock = (Block) n;
        } else {
            Node n = baseBlock.get(name.replace('/', ' ').trim(), 'X');
            if (n == null) {
                n = new Block();
                n.setName(name.replace('/', ' ').trim());
                baseBlock.add(n, false);
                changed();
            }
            currentBlock = (Block) n;
        }
        unlock(lock);
        return this;
    }

    //Read methods:
    public String get(String n) {
        long lock = lock();
        try {
            String S = currentBlock.get(n).getSValue();
            unlock(lock);
            return S;
        } catch (Exception e) {
            unlock(lock);
            return "ObjectNotFound";
        }
    }

    public String getS(String n) {
        long lock = lock();
        try {
            String S = currentBlock.get(n, 'S').getSValue();
            unlock(lock);
            return S;
        } catch (Exception e) {
            unlock(lock);
            return "ObjectNotFound";
        }
    }

    public Boolean getB(String n) {
        long lock = lock();
        try {
            boolean B = ((NodeB) currentBlock.get(n, 'B')).getValue();
            unlock(lock);
            return B;
        } catch (Exception e) {
            unlock(lock);
            return false;
        }
    }

    public double getD(String n) {
        long lock = lock();
        try {
            double D = ((NodeD) currentBlock.get(n, 'D')).getValue();
            unlock(lock);
            return D;
        } catch (Exception e) {
            unlock(lock);
            return -1.0D;
        }
    }

    public int getI(String n) {
        long lock = lock();
        try {
            int I = ((NodeI) currentBlock.get(n, 'I')).getValue();
            unlock(lock);
            return I;

        } catch (Exception e) {
            unlock(lock);
            return -1;
        }

    }

    public CFG addNew(String name, String value, char type, boolean Override) {
        long lock = lock();
        if (type == 'X') {
            currentBlock.add(new Block(name, currentBlock), Override);
            LOG.println("Added new Block [" + name + "] to " + currentBlock.getName(), "D");
        } else {
            switch (type) {
                case 'B':
                    currentBlock.add(new NodeB(name, value, currentBlock), Override);
                    LOG.println("Added new Node [" + name + " = " + value + "] to " + currentBlock.getName(), "D");
                    break;
                case 'I':
                    currentBlock.add(new NodeI(name, value, currentBlock), Override);
                    LOG.println("Added new Node [" + name + " = " + value + "] to " + currentBlock.getName(), "D");
                    break;
                case 'D':
                    currentBlock.add(new NodeD(name, value, currentBlock), Override);
                    LOG.println("Added new Node [" + name + " = " + value + "] to " + currentBlock.getName(), "D");
                    break;
                case 'S':
                    currentBlock.add(new NodeS(name, value, currentBlock), Override);
                    LOG.println("Added new Node [" + name + " = " + value + "] to " + currentBlock.getName(), "D");
                    break;
                default:
                    LOG.println("adding node [" + name + " = " + value + "] skipped, wrong type: " + type, "D");

            }
        }
        changed();
        unlock(lock);
        return this;
    }

    void changed() {
        changeChanged(true);
    }

    boolean hasChanged() {
        return changed;
    }

    void saved() {
        changeChanged(false);
    }

    private synchronized void changeChanged(boolean val) {
        changed = val;
    }

    private void delay(int n) {
        try {
            sleep(n);
        } catch (InterruptedException ex) {
            LOG.println("DCFG: Delay interrupted!", "E1");
        }
    }

    Block getBaseBlock() {
        return baseBlock;
    }

    public synchronized long lock() {
        int counter = 0;
        if (locked) {
            while (locked) {
                delay(10);

                if (counter == 10) {
                    LOG.println("DCFG: lock inaviable for over 10 retries!", "E1");
                }
                if (counter == 100) {
                    LOG.println("DCFG: lock inaviable for over 100 retries!", "E2");
                }
                if (counter == 1000) {
                    LOG.println("DCFG: lock inaviable for over 1000 retries!", "E3");
                    LOG.println("DCFG: Lock will be released", "N");
                    locked = false;
                }
                counter++;
            }
            locked = true;
            counter = 0;
        } else {
            locked = true;
        }
        currentLock = System.nanoTime();
        return currentLock;
    }

    public synchronized void unlock(long lock) {
        if (locked) {
            if (lock - currentLock == 0) {
                locked = false;
            } else {
                LOG.println("DCFG: Lock missmatch, concurency error, this problem is critical and might cause lockups", "E5");
            }
        }
    }
}
