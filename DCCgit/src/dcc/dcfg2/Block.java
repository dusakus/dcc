/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.dcfg2;

import dcc.dcfg2.nodes.Node;
import java.util.ArrayList;

/**
 *
 * @author bartek
 */
public class Block extends Node {

    ArrayList<Node> Nodes;

    public Block() {
        super("NEW", "BOX", null);
        this.Nodes = new ArrayList<>();
        TYPE = 'X';
    }

    public Block(String name, Block par) {
        super(name, "BOX", par);
        this.Nodes = new ArrayList<>();
        TYPE = 'X';
    }

    public String add(Node n, boolean override) {
        if (get(n.getName(), n.TYPE) == null) {
            Nodes.add(n);
            return "OK";
        } else {
            if (override) {
                CFG.LOG.println("Overriding node " + n.getName(), "D");
                Nodes.remove(get(n.getName(), n.TYPE));
                Nodes.add(n);
            }
        }

        return "ERR";
    }

    public Node get(String name, char type) {

        CFG.LOG.println("Block " + this.getName() + ": looking for Node " + name + ", type " + type, "D");

        for (Node n : Nodes) {
            CFG.LOG.println("Checking Node " + n.getName(), "D");
            if (n.TYPE == type) {
                CFG.LOG.println("Correct type ", "D");
                if (n.getName().equals(name)) {
                    CFG.LOG.println("Found It !", "D");
                    return n;
                } else {
                    CFG.LOG.println("Wrong Name ", "D");
                }
            } else {
                CFG.LOG.println("Wrong type ", "D");
            }
        }
        CFG.LOG.println("Block " + this.getName() + ": Node " + name + ", type " + type + " not Found", "D");
        return null;
    }

    public void del(String name, char type) {
        Nodes.remove(this.get(name, type));
    }

    public Node get(String name) {

        CFG.LOG.println("Block " + this.getName() + ": looking for Node " + name, "D");

        for (Node n : Nodes) {
            CFG.LOG.println("Checking Node " + n.getName(), "D");
            if (n.TYPE != '0') {
                CFG.LOG.println("checking... ", "D");
                if (n.getName().equals(name)) {
                    CFG.LOG.println("Found It !", "D");
                    return n;
                } else {
                    CFG.LOG.println("Wrong Name ", "D");
                }
            } else {
                CFG.LOG.println("Type 0 !!! ", "D");
            }
        }
        CFG.LOG.println("Block " + this.getName() + ": Node " + name + " not Found", "D");
        return null;
    }

}
