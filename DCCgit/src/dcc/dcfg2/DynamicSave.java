/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.dcfg2;

import dcc.DCoutputH;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @desc this class works as an active file storage, when initialized it will continuously check if CFG had changed, and if so, update the configuration file
 *
 * @author bartek
 */
public class DynamicSave extends Thread {

    File file;
    CFG cfg;
    DCoutputH log;
    boolean run;
    boolean update = true;
    String name;
    String ver;

    public DynamicSave(File f, CFG c, DCoutputH l, String n, String v) {
        file = f;
        cfg = c;
        log = l;
        this.start();
        name = n;
        ver = v;
    }

    @Override
    public void run() {
        run = true;
        while (run) {
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DynamicSave.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (update) {
                doUpdate();
                update = false;
                cfg.saved();
            }
            if (cfg.hasChanged()) {
                update = true;
            }
        }
    }

    private void doUpdate() {
        new Saver().saveToFile(cfg, file, name, ver, true);
    }

    public void update() {
        update = true;
    }

    public void end() {
        run = false;
    }
}
