/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.dcfg2;

import dcc.StData;
import dcc.dcfg2.nodes.Node;
import dcc.info_DC;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 
 *
 * @author bartek
 */
public class Saver {

    private PrintWriter outS;

    public void saveToFile(CFG in, File out, String name, String version, boolean override) {
        long lock = in.lock();
        try {
            setOutput(out, override);
        } catch (Exception ex) {
            CFG.LOG.println("Error, cant create file" + out.getAbsolutePath(), "E2");
            return;
        }
        writeHeader(name, version);
        readBlock(in.getBaseBlock());
        outS.close();
        in.unlock(lock);
    }

    private void setOutput(File out, boolean override) throws FileNotFoundException, IOException {
        if (!out.exists()) {
            out.createNewFile();
        } else {
            if (override) {
                out.createNewFile();
            } else {
                throw new IOException("Already exists, override not allowed");
            }
        }
        outS = new PrintWriter(out);
    }

    private void readNode(Node node) {
        if (!node.getName().equalsIgnoreCase("null")) {
            writeLine(node.getString());
        }
    }

    private void writeLine(String s) {
        outS.println(s);
    }

    private void writeHeader(String n, String v) {
        outS.println("<" + info_DC.CFGVer + ">");
        outS.println("<" + n + ">");
        outS.println("<" + v + ">");
        outS.println("########################");

    }

    private void readBlock(Block block) {
        writeLine("[" + block.getName());
        for (Node n : block.Nodes) {
            CFG.LOG.println(n.getName(), "D");
        }
        for (Node n : block.Nodes) {
            if (n.TYPE != '0') {
                if (n.TYPE == 'X') {
                    CFG.LOG.println("entering " + n.getName(), "D");
                    readBlock((Block) n);
                    CFG.LOG.println("leaving " + n.getName(), "D");
                } else {
                    readNode(n);
                }
            }
        }
        writeLine("]");
    }

}
