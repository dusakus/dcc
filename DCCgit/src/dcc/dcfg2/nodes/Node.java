/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.dcfg2.nodes;

import dcc.dcfg2.Block;
import dcc.dcfg2.CFG;

/**
 *
 * @author bartek
 */
public class Node {

    Block parrent;
    public char TYPE;
    String valueS = "NoValue";
    String name = "NULL";

    public Node(String name, String value, Block par) {
        TYPE = '0';
        this.name = name;
        this.valueS = value;
        parse();
        parrent = par;
    }

    public void setValue(String val) {
        valueS = val;
        parse();
    }

    void parse() {
        //nothing to od here :|
    }

    public String getSValue() {
        return new String(valueS);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getString() {
        char[] t = {TYPE};
        return (new String(t)) + ':' + name + '=' + valueS;
    }

    public Block getParrent() {
        return parrent;
    }
}
