/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 *
 */
package dcc.dcfg2.nodes;

import dcc.dcfg2.Block;

/**
 *
 * @author bartek
 */
public class NodeB extends Node {

    private Boolean value = false;

    /**
     *
     * @param name
     * @param value
     */
    public NodeB(String name, String value, Block par) {
        super(name, value, par);
        TYPE = 'B';
    }

    @Override
    void parse() {
        switch (valueS.trim()) {
            case ("true"):
                value = true;
            case ("false"):
                value = false;
        }
    }

    public boolean getValue() {
        return value;
    }
}
