/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.dcfg2.nodes;

import dcc.dcfg2.Block;

/**
 *
 * @author bartek
 */
public class NodeI extends Node {

    private int value = 0;

    public NodeI(String name, String value, Block par) {
        super(name, value, par);
        TYPE = 'I';
    }

    @Override
    void parse() {
        value = Integer.parseInt(valueS.trim());
    }

    public int getValue() {
        return value;
    }
}
