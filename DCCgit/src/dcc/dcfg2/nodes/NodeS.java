/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.dcfg2.nodes;

import dcc.dcfg2.Block;

/**
 *
 * @author bartek
 */
public class NodeS extends Node {

    public NodeS(String name, String value, Block par) {
        super(name, value, par);
        TYPE = 'S';
    }

    @Override
    public void setValue(String val) {
        valueS = val;
    }

    public String getValue() {
        return valueS;
    }
}
