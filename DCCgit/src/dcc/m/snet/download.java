package dcc.m.snet;

import dcc.StData;
import dcc.sify.MultiString;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * @author dusakusD
 */
public class download {

    public String one(String what, String where, boolean overwrite) {
        String output = "ERR";
        try {
            if (new File(where).exists()) {
                if (!overwrite) {
                    return "EXISTS";
                } else {
                    new File(where).delete();
                    new File(where).createNewFile();
                    output = "OVERWRITE";
                }
            } else {
                new File(where).createNewFile();
                output = "CREATE";
            }
            URL IN = new URL(what);
            output = "URL";
            ReadableByteChannel INch = Channels.newChannel(IN.openStream());
            output = "Ich";
            FileOutputStream OUT = new FileOutputStream(where);
            output = "Ost";
            OUT.getChannel().transferFrom(INch, 0, Long.MAX_VALUE);
            output = "OK";
        } catch (Exception e) {
            MultiString MSEO = new MultiString("        <DOWNLOAD FAIL>");
            MSEO.addL("Exception occured when downloading");
            MSEO.addL(what);
            MSEO.addL("to");
            MSEO.addL(where);
            if (overwrite) {
                MSEO.addL("with overwrite mode");
            } else {
                MSEO.addL("without overwriting");
            }
            MSEO.addL("Returning code " + output);
            StData.log.print(MSEO, "E3");
        }

        return output;
    }
}
