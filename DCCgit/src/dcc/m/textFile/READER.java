package dcc.m.textFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author dusakus
 */
public class READER {

    File F;
    BufferedReader br;

    public READER(File f) throws FileNotFoundException {
        F = f;
        br = new BufferedReader(new FileReader(F));
    }

    public String nextLine() throws IOException {
        if (br.ready()) {
            return br.readLine();
        }
        return null;
    }

    public void Reset() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(F));
    }
}
