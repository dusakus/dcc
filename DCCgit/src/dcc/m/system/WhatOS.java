/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package launcher.tools;

/**
 *
 * @author dusakusD
 */
public class WhatOS {

    private static String OS = System.getProperty("os.name").toLowerCase();

    public static String get() {
        if (isW()) {
            return "win";
        } else if (isM()) {
            return "osx";
        } else if (isU()) {
            return "unix";
        } else {
            System.out.println("I dont know your os");
            return "unknown";
        }
    }

    public static boolean isW() {
        return (OS.indexOf("win") >= 0);
    }

    public static boolean isM() {
        return (OS.indexOf("mac") >= 0);
    }

    public static boolean isU() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    }

    public static boolean isSolaris() {
        return (OS.indexOf("sunos") >= 0);
    }
}
