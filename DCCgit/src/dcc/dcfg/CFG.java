package dcc.dcfg;

import dcc.DCoutputH;
import java.io.File;

/**
 *
 * @author dusakusD
 */
public class CFG {

    public Cffile cfg;
    DCoutputH log;

    public CFG(File file, DCoutputH logI) {
        //TODO Cfg manager structure
        log = logI;
        LoadCfg loader = new LoadCfg();
        cfg = loader.load(file, log);
    }

    public boolean getB(String find) {
        Cob obj = cfg.get(find);
        return obj.valueB;
    }

    public int getI(String find) {
        Cob obj = cfg.get(find);
        return obj.valueI;
    }

    public double getD(String find) {
        Cob obj = cfg.get(find);
        return obj.valueD;
    }

    public String getS(String find) {
        Cob obj = cfg.get(find);
        return obj.valueS;
    }

}
