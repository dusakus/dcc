package dcc.dcfg;

import dcc.DCoutputH;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/*
 * TODO :
 *      -DATABLOCKS !!!
 *      -removing cells (?)
 */
public class Cffile {

    public File file;
    public String fileVer;

    public String app;
    public String appver;

    public List names;
    public List data;

    private final DCoutputH log;

    public Cffile(DCoutputH logI) {
        log = logI;
        names = new ArrayList();
        data = new ArrayList();
        names.add(0, "null");
        data.add(0, new Cob("null", "null"));
        Cob nul = (Cob) data.get(0);
        nul.valueD = 0.0;
        nul.valueI = 0;
        nul.valueB = false;
        nul.valueS = "null";
    }

    //Adding data
    // This method isn't as good as i though
    public void add(Cob in) {
        names.add(in.name);
        data.add(in);
        if (names.size() == data.size()) {
            log.println("A new data was added at " + names.size() + " called " + in.name, "D");
        } else {
            log.println("Something went terribly wrong :" + names.size() + " is not same as  " + in.name, "E5");
        }
    }

    // and these would be better than it :
    public void add(boolean value, String name) {
        if (find(name) == 1) {
            Cob out = new Cob(name, value);
            add(out);
        } else {
            get(name).valueB = value;
        }
    }

    public void add(int value, String name) {
        if (find(name) == 1) {
            Cob out = new Cob(name, value);
            add(out);
        } else {
            get(name).valueI = value;
        }
    }

    public void add(double value, String name) {
        if (find(name) == 1) {
            Cob out = new Cob(name, value);
            add(out);
        } else {
            get(name).valueD = value;
        }
    }

    public void add(String value, String name) {
        if (find(name) == 1) {
            Cob out = new Cob(name, value);
            add(out);
        } else {
            get(name).valueS = value;
        }
    }

    //Accessing data     
    public Cob get(String name) {
        int addr = 0;
        addr = find(name);
        return (Cob) data.get(addr);
    }

    private int find(String find) {
        log.println("====]Now looking for " + find, "D");
        boolean run = true;
        int id = 0;
        String checking = "NULL";
        boolean found = false;
        while (run) {
            id++;
            log.println("==]now looking at " + id, "D");
            if (id > names.size()) {
                run = false;
                return 1;
            } else {
                try {
                    checking = (String) names.get(id);
                } catch (IndexOutOfBoundsException ex) {
                    run = false;
                    log.println("Reached end of list", "D");
                }
                log.println("==]" + checking, "D");
                if (checking.equalsIgnoreCase(find)) {
                    log.println("==]found data titled " + find, "D");
                    found = true;
                } else {
                    log.println("==]" + find + " wasn't found at " + id, "D");
                }
                //return if found
                if (found == true) {
                    run = false;
                    return id;
                }
            }
        }
        return 1;
    }

    //clearing cells
    public void clear(String name) {
        Cob thing = get(name);
        thing.valueB = false;
        thing.valueD = 0.0;
        thing.valueI = 0;
        thing.valueS = "null";
        log.println("[Dcfg]===> Cell " + name + " has been cleared", "D");
    }

    //Ummm nothing
    public void test() {
        log.println(this.get("PAR").name, "D");
    }
}
