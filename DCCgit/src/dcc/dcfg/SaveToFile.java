/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.dcfg;

import dcc.StData;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author bartek
 */
public class SaveToFile {

    PrintWriter outS;

    public void saveToFile(Cffile in, File out, String name) {
        try {
            setOutput(out);
        } catch (Exception ex) {
            StData.log.println("Failed to store DATA");
        }
        writeHeader(name);
        for (Object aO : in.data) {
            Cob a = (Cob) aO;
            readCell(a);
        }
        outS.close();
    }

    private void setOutput(File out) throws FileNotFoundException, IOException {
        out.createNewFile();
        outS = new PrintWriter(out);
    }

    private void readCell(Cob cell) {
        if (!cell.name.equalsIgnoreCase("null")) {
            if (cell.valueD == 0.00D) {
            } else {
                writeLine("D", cell.name, cell.valueD + "");
            }
            if (!cell.valueS.equalsIgnoreCase("null")) {
                writeLine("S", cell.name, cell.valueS);
            }
            if (cell.valueI == 0) {
            } else {
                writeLine("I", cell.name, cell.valueI + "");
            }
            writeLine("B", cell.name, cell.valueB + "");
        }
    }

    private void writeLine(String type, String name, String in) {
        //building line
        String out;
        out = type + ":" + name + "=" + in;
        //Writing it
        outS.println(out);
    }

    private void writeHeader(String n) {
        outS.println("<0.0.0>");
        outS.println("<" + n + ">");
        outS.println("<0.0.0>");
        outS.println("########################");

    }
}
