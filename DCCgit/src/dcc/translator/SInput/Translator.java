/*
 TODO: Translattion from different versions of text files
 */
package dcc.translator.SInput;

import dcc.StData;
import dcc.sify.SimString;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author dusakus
 */
public class Translator {

    private static ArrayList<Box> Boxes;

    public static int INIT() {
        Boxes = new ArrayList<Box>();
        return 0;
    }

    public static String translate(String IN) {
        StData.log.println("[TRANSLATOR]: Trying to translate <" + IN + ">", "D");
        value ret = getValue(IN);
        if (ret != null) {
            StData.log.println("[TRANSLATOR]: Translated <" + IN + "> to <" + ret.Name + ">", "D");
            return ret.Name;
        }
        StData.log.println("[TRANSLATOR]: Tranlation for <" + IN + "> not found", "D");
        return "INVALID";
    }

    public static String translate(String IN, String CAT) {
        StData.log.println("[TRANSLATOR]: Trying to translate <" + IN + "> from category <" + CAT + ">", "D");
        Box b = getBox(CAT);
        if (b != null) {
            value v = getValue(IN, b);
            if (v != null) {
                StData.log.println("[TRANSLATOR]: Translated <" + IN + "> to <" + v.Name + ">", "D");
                return v.Name;
            }
        }
        StData.log.println("[TRANSLATOR]: translation for <" + IN + "> not found in <" + CAT + ">", "D");
        return "INVALID";
    }

    public static int LoadFile(File f) {
        Box currentBox = null;
        Name currentName = null;
        String title;

        BufferedReader br;
        if (f == null) {
            return 611;
        }

        try {
            br = new BufferedReader(new FileReader(f));
        } catch (FileNotFoundException ex) {
            StData.log.printerr(ex, "File is missing", "D");
            return 612;
        }
        boolean run = true;
        String line = "START";
        String current = "S";
        while (run) {
            try {
                line = br.readLine();
            } catch (IOException ex) {
                StData.log.printerr(ex, "File is broken", "D");
                return 613;
            }
            StData.log.println("[TRANSLATOR][file loader]>Reading line: " + line, "D");
            if (line.startsWith("[")) {
                title = SimString.rmTo(SimString.rmFrom(line, ']'), '[');
                Box b = newBox(title.trim());
                currentBox = b;
                line = "DONE";
            } else if (line.startsWith(":")) {
                if (currentBox != null) {
                    title = SimString.rmTo(SimString.rmFrom(line, '{'), ':');
                    Name n = newName(title.trim(), currentBox);
                    currentName = n;
                    line = "DONE";
                } else {
                    return 614;
                }
            } else if (line.startsWith("}")) {
                if (currentName != null) {
                    currentName = null;
                    line = "DONE";
                } else {
                    return 614;
                }
            } else if (line.startsWith("<")) {
                if (currentBox != null) {
                    currentBox = null;
                    line = "DONE";
                } else {
                    return 614;
                }
            } else if (line.startsWith("~")) {
                StData.log.println("Finished loading file", "D");
                run = false;
                line = "DONE";
            } else {
                if (currentBox != null && currentName != null) {
                    newValue(line, currentName, currentBox);
                    line = "DONE";
                }

            }
            /*switch (current) {
             case ('['):
             title = SimString.rmTo(SimString.rmFrom(line, ']'), '[');
             Box b = newBox(title.trim());
             currentBox = b;
             line = "DONE";
            
             case (':'):
             if (currentBox != null) {
             title = SimString.rmTo(SimString.rmFrom(line, '{'), ':');
             Name n = newName(title.trim(), currentBox);
             currentName = n;
             line = "DONE";
             } else {
             return 614;
             }
            
             case ('}'):
             if (currentName != null) {
             currentName = null;
             line = "DONE";
             } else {
             return 614;
             }
            
             case ('<'):
             if (currentBox != null) {
             currentBox = null;
             line = "DONE";
             } else {
             return 614;
             }
             case ('~'):
             StData.log.println("Finished loading file", "D");
             run = false;
             line = "DONE";
            
             default:
             if (currentBox != null && currentName != null) {
             newValue(line, currentName, currentBox);
             line = "DONE";
             }
            
             }*/

        }

        return 0;
    }

    private static Box newBox(String a) {
        StData.log.println("Trying to create Box <" + a + ">", "D");
        if (getBox(a) == null) {
            Box box = new Box(a);
            Boxes.add(box);
            StData.log.println("Box <" + a + "> created", "D");
            return box;
        }
        StData.log.println("Box <" + a + "> already exists", "D");
        return getBox(a);
    }

    private static Name newName(String a, Box b) {
        StData.log.println("Trying to create Name <" + a + "> in box <" + b.Box + ">", "D");
        if (b != null) {
            if (getName(a, b) == null) {
                Name name = new Name(a, b.Box);
                b.names.add(name);
                StData.log.println("created Name <" + a + "> in box <" + b.Box + ">", "D");
                return name;
            }
            StData.log.println("Name <" + a + "> in box <" + b.Box + "> already exists", "D");
            return getName(a, b);
        }
        StData.log.println("Wrong Input, Box is null", "D");
        return null;

    }

    private static value newValue(String a, Name b, Box c) {
        StData.log.println("Trying to create value <" + a + "> for name <" + b.Name + ">", "D");
        if (c != null && b != null) {
            if (getValue(a, b, c) == null) {
                value value = new value(a, b.Name, b.Box);
                b.values.add(value);
                StData.log.println("created value <" + a + "> for name <" + b.Name + ">", "D");
                return value;
            }
            StData.log.println("value <" + a + "> in name <" + b.Name + "> already exists", "D");
            return getValue(a, b, c);
        }
        StData.log.println("Wrong input, null detected", "D");
        return null;
    }

    public static Box getBox(String search) {
        StData.log.println("[TRANSLATOR]==> Now looking for Box <" + search + ">", "D");
        if (!Boxes.isEmpty()) {
            for (Box b : Boxes) {
                if (b.Box.equals(search)) {
                    StData.log.println("[TRANSLATOR]=> Box <" + search + "> found", "D");
                    return b;
                }
            }
        }
        StData.log.println("[TRANSLATOR]!!!> Box <" + search + "> not found", "D");
        return null;

    }

    public static Name getName(String search) {
        return null;

    }

    public static Name getName(String search, String Box) {
        return null;

    }

    public static Name getName(String search, Box Box) {
        StData.log.println("[TRANSLATOR]==> Now looking for Name <" + search + "> in Box <" + Box.Box + ">", "D");
        if (!Box.names.isEmpty()) {
            for (Name b : Box.names) {
                if (b.Name.equals(search)) {
                    StData.log.println("[TRANSLATOR]=> Name <" + search + "> found", "D");
                    return b;
                }
            }
        }
        StData.log.println("[TRANSLATOR]!!!> Name <" + search + "> not found", "D");
        return null;

    }

    public static value getValue(String search) {
        StData.log.println("[TRANSLATOR]====> Wide search started for value <" + search + ">");
        if (!Boxes.isEmpty()) {
            for (Box b : Boxes) {
                if (!b.names.isEmpty()) {
                    StData.log.println("[TRANSLATOR]===> Looking into Box <" + b.Box + ">", "D");
                    for (Name c : b.names) {
                        if (!c.values.isEmpty()) {
                            StData.log.println("[TRANSLATOR]==> Looking into Name <" + c.Name + ">", "D");
                            for (value d : c.values) {
                                if (d.value.equalsIgnoreCase(search)) {
                                    StData.log.println("[TRANSLATOR]=> Value <" + search + "> found", "D");
                                    return d;
                                }
                            }
                        }

                    }
                }
            }
        }
        return null;

    }

    public static value getValue(String search, Name Name, Box Box) {
        StData.log.println("[TRANSLATOR]==> Now looking for Value <" + search + "> in Name <" + Name.Name + ">", "D");
        for (value b : Name.values) {
            if (b.value.equalsIgnoreCase(search)) {
                StData.log.println("[TRANSLATOR]=> Value <" + search + "> found", "D");
                return b;
            }
        }
        StData.log.println("[TRANSLATOR]!!!> Value <" + search + "> not found", "D");
        return null;

    }

    public static value getValue(String search, Box Box) {
        StData.log.println("[TRANSLATOR]==> Now looking for Value <" + search + "> in Box <" + Box.Box + ">", "D");
        for (Name c : Box.names) {
            StData.log.println("[TRANSLATOR]==> Looking into Name <" + c.Name + ">", "D");
            for (value d : c.values) {
                if (d.value.equalsIgnoreCase(search)) {
                    StData.log.println("[TRANSLATOR]=> Value <" + search + "> found", "D");
                    return d;
                }
            }
        }
        StData.log.println("[TRANSLATOR]!!!> Value <" + search + "> not found", "D");
        return null;
    }
}
