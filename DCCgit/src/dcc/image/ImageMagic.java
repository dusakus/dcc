package dcc.image;

import dcc.StData;

/**
 *
 * @author dusakus
 *
 * This class will do various things with pixels[][] array
 */
public class ImageMagic {

    DCimgObj img;

    ImageMagic(DCimgObj Iimg) {
        img = Iimg;
        StData.log.println("Image magic created for: " + img.name, "D");
    }
}
