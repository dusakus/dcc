package dcc.image;

import dcc.image.parts.Pixel;
import dcc.sApp.DCec;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dusakus
 */
public class DCimgObj extends DCec {

    BufferedImage img;
    ImageMagic magic;
    public ArrayList<ArrayList<Pixel>> pixels;

    public DCimgObj(BufferedImage imgI) {
        img = imgI;
        pixels = new ArrayList<ArrayList<Pixel>>(img.getWidth());
        readIMG();
        magic = new ImageMagic(this);
    }

    public short gColor(String select, int x, int y) {
        int in = img.getRGB(x, y);
        System.out.println(in);
        short oa = (short) ((in >>> 24) & 0xff);
        short or = (short) ((in >>> 16) & 0xff);
        short og = (short) ((in >>> 8) & 0xff);
        short ob = (short) (in & 0xff);
        switch (select) {
            case "a":
                return oa;
            case "r":
                return or;
            case "g":
                return og;
            case "b":
                return ob;
            default:
                return 0;
        }

    }

    private void readIMG() {
        InitArray();
        int x = 0;
        int y = 0;
        for (ArrayList<Pixel> AP : pixels) {
            for (Pixel P : AP) {
                readPixel(x, y, P);
                y++;
            }
            x++;
        }
        //Image will be loaded to pixels list here
    }

    private void InitArray() {
        for (int i = 0; i == img.getWidth(); i++) {
            pixels.add(new ArrayList<Pixel>(img.getHeight()));
        }
    }

    private void readPixel(int x, int y, Pixel P) {
        int in = img.getRGB(x, y);
        P.a = (short) ((in >>> 24) & 0xff);
        P.r = (short) ((in >>> 16) & 0xff);
        P.g = (short) ((in >>> 8) & 0xff);
        P.b = (short) (in & 0xff);
    }

    @Override
    public boolean tick() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean init() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean stop() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
