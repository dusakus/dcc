/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.gaeng;

import java.awt.Dimension;

/**
 *
 * @author bartek
 */
public class GameSetup {

    //Window properties
    public int width = 80;
    public int height = 60;
    public int scaling = 8;
    public String title = "Unnamed";
    
    //Rendering settings
    public int bufferCount = 2;
    public int TPS = 200;

    //Debug only stuff
    public boolean speedMeterEnabled = true;
    public boolean extendedOutput = false;
    
    /*
     */
    public Dimension getWSize() {
        return new Dimension(width * scaling-10, height * scaling-10);
    }
    public boolean EO(){
        return extendedOutput;
    }
}
