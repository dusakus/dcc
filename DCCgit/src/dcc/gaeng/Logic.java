/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng;

import dcc.sApp.DCec;

/**
 *
 * @author bartek
 */
public class Logic extends DCec{
    private Game G;
    public Logic(Game aThis){
        this.name = "GameLogic";
        G = aThis;
    }
    public boolean userInit(){
        return false;
    }
}
