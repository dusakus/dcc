/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.gaeng;

import dcc.DCoutputH;
import dcc.sApp.Loop;
import dcc.sApp.tools.SpeedMeter;
import dcc.sify.boxes.LongB;

/**
 *
 * @author bartek
 */
public class Game {

    public Loop LOOP;
    public DCoutputH LOG;
    public WindowCon WIN;
    LongB WINid;
    public Logic L;
    LongB Lid;
    public GameSetup GS;
    public GameRegistry GR;

    public void prepare(DCoutputH log, GameSetup gs) {
        LOG = log;
        
        GS = gs;
        
        
        WIN = new WindowCon(this);

        LOOP = new Loop(LOG, GS.TPS);
        L = new Logic(this);
        WINid = LOOP.add(WIN);
        Lid = LOOP.add(L);
        GR = new GameRegistry(this);
        LOOP.add(GR.GIH);
        
        log.println("Gaeng-Game: Preparation completted");
    }

    public void start(long max) {
        
        
        if(GS.speedMeterEnabled){
            LOOP.add(new SpeedMeter(LOG));
        }
        LOOP.start(max);
        stop();
    }
    public void stop(){
        LOG.println("Gaeng-Game: Stop method called, exitting");
        LOOP.stop();
        LOG.END("Gaeng-Game: BaYo!");
        System.exit(0);
    }
}
