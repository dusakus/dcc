/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.logic.interfaces;

import dcc.gaeng.render.interfaces.IMoveableInt;

/**
 *
 * @author bartek
 */
public interface ISimpleLogic {
    abstract void update(IMoveableInt OBJ);
}
