/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.logic.minis;

import dcc.gaeng.Game;
import dcc.gaeng.render.interfaces.IMoveableInt;
import dcc.gaeng.logic.interfaces.ISimpleLogic;

/**
 *
 * @author bartek
 */
public class DisableAt implements ISimpleLogic{

    int tick =-1;
    Game G;
    
    public DisableAt(Game g, int t){
        tick = t;
        G = g;
    }
    
    @Override
    public void update(IMoveableInt OBJ) {
        if (G.LOOP.getTCount() == tick){
            OBJ.setDisabled(true);
        }
    }
    
}
