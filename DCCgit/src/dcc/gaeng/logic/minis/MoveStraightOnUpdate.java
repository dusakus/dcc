/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.logic.minis;

import dcc.gaeng.Game;
import dcc.gaeng.render.interfaces.IMoveableInt;
import dcc.gaeng.logic.interfaces.ISimpleLogic;
import dcc.gaeng.render.parts.dynamicSingleLayer;

/**
 *
 * @author bartek
 */
public class MoveStraightOnUpdate implements ISimpleLogic{
    Game G;
    int Xmod = 0;
    int Ymod = 0;
    
    public MoveStraightOnUpdate(Game g,int X, int Y){
        G = g;
        Xmod = X;
        Ymod = Y;
    }
    @Override
    public void update(IMoveableInt OBJ) {
        OBJ.setX(OBJ.getX()+Xmod*G.GS.scaling);
        OBJ.setY(OBJ.getY()+Ymod*G.GS.scaling);
    }
    public void update(IMoveableInt OBJ,int changeX, int changeY, boolean Remember) {
        OBJ.setX(OBJ.getX()+changeX*G.GS.scaling);
        OBJ.setY(OBJ.getY()+changeY*G.GS.scaling);
        if(Remember){
            Xmod = changeX;
            Ymod = changeY;
        }
    }
    
}
