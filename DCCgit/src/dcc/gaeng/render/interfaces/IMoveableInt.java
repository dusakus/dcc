/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.render.interfaces;

/**
 *
 * @author bartek
 */
public interface IMoveableInt {
    abstract boolean isDisabled();
    abstract void setDisabled(boolean nv);
    abstract int getX();
    abstract int getY();
    abstract void setX(int nv);
    abstract void setY(int nv);
}
