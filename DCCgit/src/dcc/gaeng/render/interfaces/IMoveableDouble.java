/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.render.interfaces;

/**
 *
 * @author bartek
 */
public interface IMoveableDouble {
    abstract boolean isEnabled();
    abstract double getX();
    abstract double getY();
    abstract void setX(double nv);
    abstract void setY(double nv);
}
