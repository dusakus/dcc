/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.gaeng.render;

import dcc.gaeng.Game;
import dcc.gaeng.render.parts.IDrawable;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author bartek
 */
public class Screen {
    private Game G;
    
    ArrayList<IDrawable> renderList;
    
    public Screen(Game g){
        G = g;
        renderList = new ArrayList<>();
    }
    public void add(IDrawable o){
        o.init(G);
        renderList.add(o);
    } 

    public void draw(Graphics grf,BufferedImage bi, int[] pixels) {
        for(IDrawable o : renderList){
            if(o.isEnabled()){
                if(G.GS.EO()){
                    G.LOG.println("Gaeng-Screen: drawing "+o.getName(), "D");
                }
                o.draw(grf,bi,pixels);
            }
                o.update();
        }
    }
    public void remove(IDrawable o){
        renderList.remove(o);
    }
    public void clear(){
        renderList.clear();
    }
    

}
