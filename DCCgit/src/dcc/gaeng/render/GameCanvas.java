/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.gaeng.render;

import dcc.gaeng.Game;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

/**
 *
 * @author bartek
 */
public class GameCanvas extends Canvas {

    private Game G;

    private BufferedImage GRF;
    private int[] pixels;

    public GameCanvas(Game g) {
        G = g;
        setMinimumSize(G.GS.getWSize());
        setMaximumSize(G.GS.getWSize());
        setPreferredSize(G.GS.getWSize());

        GRF = new BufferedImage(G.GS.width, G.GS.height, BufferedImage.TYPE_INT_ARGB);
        pixels = ((DataBufferInt) GRF.getRaster().getDataBuffer()).getData();
    }

    public void init() {
        GRF = new BufferedImage(G.GS.width, G.GS.height, BufferedImage.TYPE_INT_ARGB);
        pixels = ((DataBufferInt) GRF.getRaster().getDataBuffer()).getData();
        G.LOG.println("Gaeng-GameCanvas: Initialization done");
    }

    public void render() {
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(G.GS.bufferCount);
            bs = getBufferStrategy();
        }
        Graphics grf = bs.getDrawGraphics();
        grf.setColor(Color.BLACK);
        grf.fillRect(0, 0, G.GS.getWSize().width, G.GS.getWSize().height);
        grf.drawImage(scaleFrame(GRF), 0, 0, null);

        G.GR.draw(grf, GRF, pixels);

        //for (int i = 0; i < pixels.length; i++) {
        //pixels[i] = Color.BLUE.getRGB();
        //   }
        

        grf.dispose();
        bs.show();
    }

    private Image scaleFrame(BufferedImage GRF) {
        /*BufferedImage scaledFrame = new BufferedImage(G.GS.width*G.GS.scaling, G.GS.height*G.GS.scaling, BufferedImage.TYPE_INT_ARGB);
        Graphics g = scaledFrame.createGraphics();
        g.drawImage(GRF.getScaledInstance(G.GS.width*G.GS.scaling, G.GS.height*G.GS.scaling, Image.SCALE_REPLICATE), 0, 0, new Color(0,0,0), null);
        g.dispose();*/
        return GRF.getScaledInstance(G.GS.width*G.GS.scaling, G.GS.height*G.GS.scaling, Image.SCALE_REPLICATE);
    }

}
