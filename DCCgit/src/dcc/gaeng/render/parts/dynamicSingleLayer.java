/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.gaeng.render.parts;

import dcc.gaeng.render.interfaces.IMoveableInt;
import dcc.gaeng.logic.interfaces.ISimpleLogic;
import dcc.gaeng.logic.minis.MoveStraightOnUpdate;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author bartek
 */
public class dynamicSingleLayer extends SingleLayer implements IMoveableInt {

    ArrayList<ISimpleLogic> logic;

    public dynamicSingleLayer(String n, boolean e, BufferedImage TEX, boolean Uu) {
        super(n, e, TEX, false);
        logic = new ArrayList<>();
    }

    public void addLogic(ISimpleLogic l) {
        logic.add(l);
    }

    public void update() {
        for (ISimpleLogic l : logic) {
            l.update(this);
        }
    }

    public int getX() {
        return posX;
    }

    public int getY() {
        return posY;
    }

    public void setX(int nv) {
        if (nv > G.GS.width * G.GS.scaling && enabled) {
            enabled = false;
        } else {
            enabled = true;
        }
        if (nv < texture.getWidth() * G.GS.scaling * -1 && enabled) {
            enabled = false;
        } else {
            enabled = true;
        }
        posX = nv;
    }

    public void setY(int nv) {
        if (nv > G.GS.width * G.GS.scaling && enabled) {
            enabled = false;
        } else {
            enabled = true;
        }
        if (nv < texture.getHeight() * G.GS.scaling * -1 && enabled) {
            enabled = false;
        } else {
            enabled = true;
        }
        posY = nv;
    }

    @Override
    public boolean isDisabled() {
        return Fdisabled;
    }

    @Override
    public void setDisabled(boolean nv) {
        Fdisabled = nv;
    }

}
