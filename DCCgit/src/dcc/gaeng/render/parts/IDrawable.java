/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.render.parts;

import dcc.gaeng.Game;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 *
 * @author bartek
 */
public interface IDrawable {

    /**
     *
     * @return returns name of drawable object
     */
    abstract String getName();

    /**
     * Initialize drawable object
     *
     * @param G game object of drawable
     */
    abstract void init(Game G);

    /**
     * Ask object to draw itself onto given gaphics
     *
     * @param grf graphics object to draw to
     * @param bi BufferedImage to draw onto
     * @param pixels pixel array to draw onto
     */
    abstract void draw(Graphics grf,BufferedImage bi, int[] pixels);
    abstract void update();
    abstract boolean isEnabled();
    abstract void setEnabled(boolean e);
}
