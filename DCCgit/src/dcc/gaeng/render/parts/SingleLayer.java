/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.render.parts;

import dcc.gaeng.Game;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;

/**
 *
 * @author bartek
 */
public class SingleLayer implements IDrawable{
    boolean enabled = true;
    boolean Fdisabled = false;
    String name = "New singleLayer";
    Game G;
    BufferedImage texture;
    Image wtexture;
    byte[] Tpixels;
    boolean wholeFrame = false;
    
    int posX = 0;
    int posY = 0;
    
    public SingleLayer(String n, boolean e, BufferedImage TEX,boolean wf){
        name = n;
        enabled = e;
        Fdisabled = !e;
        texture = TEX;
        wholeFrame = wf;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void init(Game g) {
        g.LOG.println("SingleLayer initialization");
        G = g;
        if(wholeFrame){
            wtexture = texture.getScaledInstance(G.GS.width*G.GS.scaling, G.GS.height*G.GS.scaling, Image.SCALE_REPLICATE);
        }
        Tpixels = ((DataBufferByte)texture.getRaster().getDataBuffer()).getData();
    }

    @Override
    public void draw(Graphics grf, BufferedImage bi, int[] pixels) {
        if (texture != null && !Fdisabled) {
            if(wholeFrame){
                grf.drawImage(wtexture, 0, 0, null);
            }else{
                grf.drawImage(texture.getScaledInstance(texture.getWidth()*G.GS.scaling, texture.getHeight()*G.GS.scaling, Image.SCALE_REPLICATE), posX, posY, null);
            }
        }
        
    }

    @Override
    public void update() {
        
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean e) {
        enabled = e;
    }
    
}
