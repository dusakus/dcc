/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.gaeng;

import dcc.dcfg2.CFG;
import dcc.gaeng.input.GameInputHandler;
import dcc.gaeng.render.Screen;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author bartek
 */
public class GameRegistry {
    
    Game G;
    Screen activeScreen;
    Screen overlay;
    CFG cfg;
    
    private ArrayList<Screen> screens;
    
    public GameInputHandler GIH;
    
    GameRegistry(Game aThis) {
        G = aThis;
        screens = new ArrayList<>();
        cfg = new CFG(G.LOG);
        cfg.addNew("GAMECFG", "NEWGAMECFG", 'S', true);
        GIH = new GameInputHandler(G);
    }
    
    public void draw(Graphics grf, BufferedImage bi, int[] pixels) {
        if (activeScreen != null) {
            activeScreen.draw(grf, bi, pixels);
        }
        if (overlay != null) {
            overlay.draw(grf, bi, pixels);
        }
    }
    public void add(Screen s){
        screens.add(s);
    }
    public void selectScreen(int id){
        try {
            activeScreen = screens.get(id);
        } catch (Exception e) {
            G.LOG.printerr(e, "Gaeng-GameRegistry: Screen "+id+" doesn't exist", "E3");
        }
    }
    public void setOverlay(Screen s){
        overlay = s;
    }
    
}
