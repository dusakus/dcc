/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng;

import dcc.gaeng.input.keyboard.KeyboardListener;
import dcc.gaeng.render.GameCanvas;
import dcc.sApp.DCec;
import java.awt.BorderLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;
import javax.swing.JFrame;

/**
 *
 * @author bartek
 */
public class WindowCon extends DCec{
    private Game G;
    
    private JFrame window;
    private GameCanvas GC;
    
    WindowCon(Game aThis) {
        this.name = "GameWindowCon";
        G =aThis;
        GC = new GameCanvas(G);
        window = new JFrame(G.GS.title);
        addExitListener();
        window.setLayout(new BorderLayout());
        window.add(GC,BorderLayout.CENTER);
        
        
    }
    public boolean userInit(){
        GC.init();
        window.pack();
        window.setResizable(false);
        window.setLocationRelativeTo(null);
        window.setVisible(true);
        
        return false;
    }
    public boolean userTick(){
        GC.render();
        
        return false;
    }
    public boolean userStop(){
        return false;
    }
    private void addExitListener(){
        window.addWindowListener(new WindowListener() {
            //I skipped unused callbacks for readability

            @Override
            public void windowClosing(WindowEvent e) {
                G.stop();
            }

            @Override
            public void windowOpened(WindowEvent e) {
                
            }

            @Override
            public void windowClosed(WindowEvent e) {
                
            }

            @Override
            public void windowIconified(WindowEvent e) {
                
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
                
            }

            @Override
            public void windowActivated(WindowEvent e) {
                
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
                
            }
        });
    }

    public void addKbdListener(KeyboardListener KL) {
        window.addKeyListener(KL);
    }
}
