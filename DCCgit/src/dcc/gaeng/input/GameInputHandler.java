/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.input;

import dcc.gaeng.Game;
import dcc.gaeng.input.keyboard.KeyboardHub;
import dcc.gaeng.input.mouse.MouseHub;
import dcc.sApp.DCec;

/**
 *
 * @author bartek
 */
public class GameInputHandler extends DCec{
    public KeyboardHub KH;
    public MouseHub MH;
    private Game G;
    
    public GameInputHandler(Game g){
        
        G = g;
        KH = new KeyboardHub(G, this);
        MH = new MouseHub(G, this);
    }
    
    public boolean userInit(){
        
        
        
        return false;
        
    }
}
