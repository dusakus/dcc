/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.input.keyboard;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author bartek
 */
public class KeyboardListener implements KeyListener{
    KeyboardHub KH;

    KeyboardListener(KeyboardHub aThis) {
        KH = aThis;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        KH.keyPressed(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        KH.keyReleased(e.getKeyCode());
    }
    
}
