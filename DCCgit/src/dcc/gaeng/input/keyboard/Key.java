/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.gaeng.input.keyboard;

/**
 *
 * @author bartek
 */
public class Key {
    public final int KEYCODE;

    boolean isPressed = false;
    int bufferCounter = 0;
    int keyPressCount = 0;

    Key(int keyCode) {
        KEYCODE = keyCode;
    }

    public void pressed() {
        isPressed = true;
        bufferCounter++;
        keyPressCount++;
    }

    public void released() {
        isPressed = false;
    }

    public boolean isPressed() {
        return isPressed;
    }

    public int getBuffer(boolean reset) {
        if (reset) {
            int out = bufferCounter;
            bufferCounter = 0;
            return out;
        }
        return bufferCounter;
    }
    public int getPressCount(){
        return keyPressCount;
    }
}
