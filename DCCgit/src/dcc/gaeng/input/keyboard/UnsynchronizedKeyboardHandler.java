/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */
package dcc.gaeng.input.keyboard;

import java.util.ArrayList;

/**
 *
 * @author bartek
 */
public class UnsynchronizedKeyboardHandler {

    private ArrayList<IKeyboardListener> listeners;
    
    public UnsynchronizedKeyboardHandler(){
        listeners = new ArrayList<>();
    }

    void keyPressed(int keyCode) {
        synchronized(this) {            
            for (IKeyboardListener kl : listeners) {
                kl.keyPressed(keyCode);
            }
        }
    }

    void keyReleased(int keyCode) {
        synchronized(this) {            
            for (IKeyboardListener kl : listeners) {
                kl.keyReleased(keyCode);
            }
        }
    }

    void addHandler(IKeyboardListener h) {
        synchronized(this){
            listeners.add(h);
        }
    }
}
