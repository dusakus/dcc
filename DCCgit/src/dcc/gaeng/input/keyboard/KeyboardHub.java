/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.input.keyboard;

import dcc.gaeng.Game;
import dcc.gaeng.input.GameInputHandler;

/**
 *
 * @author bartek
 */
public class KeyboardHub {
    private KeyboardListener KL;
    public SynchronizedKeyboardHandler SKH;
    public UnsynchronizedKeyboardHandler USKH;
    private Game G;
    private GameInputHandler GIH;
    
    public Key[] keys;
    
    
    
    public KeyboardHub(Game g, GameInputHandler gih){
        G = g;
        GIH = gih;
        KL = new KeyboardListener(this);
        SKH = new SynchronizedKeyboardHandler();
        USKH = new UnsynchronizedKeyboardHandler();
        G.WIN.addKbdListener(KL);
        keys = new Key[2048];
    }

    void keyPressed(int keyCode) {
        if(keys[keyCode] == null) keys[keyCode] = new Key(keyCode);
        keys[keyCode].pressed();
        USKH.keyPressed(keyCode);
    }

    void keyReleased(int keyCode) {
        keys[keyCode].pressed();
        USKH.keyReleased(keyCode);
    }
    public void addUSyncHandler(IKeyboardListener h){
        this.USKH.addHandler(h);
    }
}
