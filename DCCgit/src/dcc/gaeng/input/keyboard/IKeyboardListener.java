/*
 *     <---======[ D-LIC open code 0.3 ]======--->
 *          This file is part of a project
 *          licensed with the DLoc license.
 *  Project's folder should contain the license file
 *                      _______
 *                     /dusakus\
 *         <=>---<>--{2013 stuDIO}--<>---<=>
 * 
 */

package dcc.gaeng.input.keyboard;

/**
 *
 * @author bartek
 */
public interface IKeyboardListener {

    abstract public void keyPressed(int keyCode);
    abstract public void keyReleased(int keyCode);
    
}
