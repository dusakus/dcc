package dcc.sify;

import java.util.ArrayList;

/**
 * This project is Licensed under D-LIC-opencode License.
 *
 * @author dusakus
 */
public class SimCompare {

    public static ArrayList<String> AL_a_sub_b(ArrayList<String> a, ArrayList<String> b) {
        ArrayList<String> c = (ArrayList<String>) a.clone();

        for (String S : a) {
            for (String S1 : b) {
                if (S.equals(S1)) {
                    c.remove(S);
                }
            }
        }
        return c;
    }
}
