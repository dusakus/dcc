package dcc.sify;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 *
 * @author dusakus
 */
public class SimStream {

    public synchronized static void copy(URL in, File out) throws IOException {
        /*OLD but working
         * 
         * String line = null;
         BufferedReader Ain = new BufferedReader(new FileReader(in.getFile()));
         line = Ain.readLine();
         PrintWriter Aout = new PrintWriter(out);
         while (line != null){
         Aout.println(line);
         line = Ain.readLine();
         }
         Aout.close();
         */
        try {

            InputStream inStr = new BufferedInputStream(new FileInputStream(in.getFile()));
            OutputStream extr = new BufferedOutputStream(new FileOutputStream(out));
            byte[] buffer = new byte[2048];
            for (;;) {
                int nBytes = inStr.read(buffer);
                if (nBytes <= 0) {
                    break;
                }
                extr.write(buffer, 0, nBytes);
            }
            extr.flush();
            extr.close();
            inStr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
